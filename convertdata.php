<?php
require_once 'vendor/autoload.php';
date_default_timezone_set('America/Chicago');
$client = new MongoDB\Client;
$db = $client->devworx;

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "converteddata";
// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM shifts";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $data = $row;
      //$utc_shiftdate = new DateTime($row['ShiftDate'],new DateTimeZone("UTC"));
      $_shiftdate = new DateTime($row['ShiftDate']);
      $utc_shiftdate = new DateTime($_shiftdate->format('m/d/y'),new DateTimeZone("UTC"));
      $utc_startdate = new DateTime($row['StartDate'],new DateTimeZone("UTC"));
      $utc_enddate = new DateTime($row['EndDate'],new DateTimeZone("UTC"));
      $data['ShiftDate'] = new Mongodb\BSON\UTCDateTime($utc_shiftdate);
      $data['StartDate'] = new Mongodb\BSON\UTCDateTime($utc_startdate);
      $data['EndDate'] = new Mongodb\BSON\UTCDateTime($utc_enddate);
     $db->shifts->insertOne($data);
     var_dump($data);
    }
} else {
    echo "0 results";
}
$conn->close();