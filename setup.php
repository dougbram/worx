<?php
require_once 'init.php';
$newuser = new User();
  $newuser->username = 'dbrammer';
  $newuser->firstname = 'Doug';
  $newuser->lastname = 'Brammer';
  $newuser->cellphone = '7852850311';
  $newuser->email = 'dbrammer@teamhuber.com';
  $newuser->certification = 'AEMT';
    
      array_push($newuser->roles, 'admin');
      array_push($newuser->roles, 'user');
      
  $newuser->password = password_hash('password', PASSWORD_DEFAULT);
 $result = $db->users->insertOne($newuser);

 $data =[
     [
     'position' => 'D-2',
     'nextday' => false,
     'hours' => 12,
     'starttime' => '07:00',
     'enddtime' => '19:00'
     ],
      [
     'position' => 'D-3',
     'nextday' => false,
     'hours' => 12,
     'starttime' => '07:00',
     'enddtime' => '19:00'
     ],
      [
     'position' => 'D-4',
     'nextday' => false,
     'hours' => 12,
     'starttime' => '07:00',
     'enddtime' => '19:00'
     ],
      [
     'position' => 'D-1',
     'nextday' => false,
     'hours' => 12,
     'starttime' => '07:00',
     'enddtime' => '19:00'
     ],
     
       [
     'position' => 'N-1',
     'nextday' => true,
     'hours' => 12,
     'starttime' => '19:00',
     'enddtime' => '07:00'
      ],
       [
     'position' => 'N-2',
     'nextday' => true,
     'hours' => 12,
     'starttime' => '19:00',
     'enddtime' => '07:00'
      ],
       [
     'position' => 'N-3',
     'nextday' => true,
     'hours' => 12,
     'starttime' => '19:00',
     'enddtime' => '07:00'
      ],
       [
     'position' => 'N-4',
     'nextday' => true,
     'hours' => 12,
     'starttime' => '19:00',
     'enddtime' => '07:00'
      ]
      
 ];

 $db->shiftnames->insertMany($data);

// var_dump($db->shifts->findOne(['_id' => new Mongodb\BSON\ObjectID('5b808fd7037bfa3910001c42')]));

 

 //5b808be8037bfa3910001c40