<?php

function validate_password($password,$confirmpassword){
     if(!$password == "" && !$confirmpassword == ""){
        $pw = filter_var($password);
        $cpw = filter_var($confirmpassword);
         //check if password matches
        if(!$pw === $cpw){
            create_flashmessage("danger", "Passwords do not match");
            return true;
        }
    }else{
        return false;
    }
}

function validate_username($username,$currentUserNames){
    foreach ($currentUserNames as $u){
        if($username == $u['username']){
            return true;
               
        }
    }   
        return FALSE;
}

function isLoggedIn(){
    if(isset($_SESSION['user'])){
     return true;
 }
 return;
}

function hasRole($role){
    
    if(isset($_SESSION['user'])){
        $currentUser = new User();
        $currentUser = $_SESSION['user'];
        $roles = iterator_to_array($currentUser->roles);
        if(in_array($role, $roles)){
            return TRUE;
        }
    }    
    return FALSE;
}

function userHasRole($user,$role){
    $roles = iterator_to_array($user->roles);
        if(in_array($role, $roles)){
            return TRUE;
        }
        return FALSE;
}

function get_shifts($db,$date){   
    $dt_string = $date->format('m/d/y').' 00:00:00.000';
    $queryDate = new DateTime($dt_string,new DateTimeZone('UTC'));
    $options = [
        'sort' => [
            'Position' => 1,
            'StartDate'=>1
            ]
    ];
    $results = $db->shifts->find(['ShiftDate'=> new Mongodb\BSON\UTCDateTime($queryDate)],$options);
    return $results;

}

function get_local_datetime($utc){
    return $utc->toDateTime();
}

function display_local_date($utc,$format){
    if($format == 1){
        $result = $utc->toDateTime();
        return $result->format('Y-m-d');
    }else{
        return $result->format('m-d-Y');
    }
}

function display_local_time($utc){
        $result = $utc->toDateTime();        
        return $result->format('h:i');
    
}

function get_roster($db){
    return $db->users->find([],['sort'=>['lastname'=>1,'firstname'=>1]]);
}

function get_shift_names($db){
    
    return $db->shiftnames->find([],['sort'=>
    ['position'=>1]    
    ]);
}
