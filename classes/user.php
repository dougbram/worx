<?php

class User {
    public $username ;
    public $firstname;
    public $lastname;
    public $email;
    public $cellphone;
    public $certification;
    public $password;
    public $id;
    public $roles;
    
    function __construct() {
        $this->roles = [];
    }

    function save() {
        $insertOneResult = $db->users->insertOne([
            'username' => $this->username,
            'email' => $this->email,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'cellphone' => $this->cellphone,
            'certification' => $this->certification,
            'password' => $this->password,
            'roles' => $this->roles
        ]);
        $this->id = $insertOneResult->getInsertedId();
    }
    
    function login($userdocument){
        $this->username = $userdocument['username'];
        $this->email = $userdocument['email'];
        $this->firstname = $userdocument['firstname'];
        $this->lastname = $userdocument['lastname'];
        $this->cellphone = $userdocument['cellphone'];
        $this->certification = $userdocument['certification'];
        $this->roles = $userdocument['roles'];
        $this->id = $userdocument['_id'];
        return;
    }
    
    function addRole($rolename){
        array_push($rolename);
        
    }
    
    function update(){
               
        $updateResult = $db->users->updateOne(
            ['_id' => $this->id],
            ['$set' => $this]
           );
        return $updateResult;
    }
}