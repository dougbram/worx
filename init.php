<?php //DO NOT DELETE THIS FILE.
 require_once 'z_us_root.php';
require_once 'vendor/autoload.php';
require_once $abs_us_root.$us_url_root.'classes/user.php';
require_once $abs_us_root.$us_url_root.'classes/helpers.php';
require_once $abs_us_root.$us_url_root.'classes/redirect.php';

//TODO:  GET db name from config file
//$dbname = 'devems';
//$appname = 'EMSWorx';

if(file_exists($abs_us_root.$us_url_root.'config.json')){
    $str = file_get_contents($abs_us_root.$us_url_root.'config.json');
    $_config = json_decode($str, true);
    $appname = $_config['appname'];
    $dbname = $_config['dbname'];
    date_default_timezone_set($_config['timezone']);
}else{
    $dbname = 'newapp';
    $appname="new app";
    date_default_timezone_set('America/Chicago');
}

//TODO: GET timezone from config file
date_default_timezone_set('America/Chicago');
 if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }


$client = new MongoDB\Client;
$db = $client->$dbname;

if (!session_status() == PHP_SESSION_NONE) {
    if(!isset($_SESSION['messages'])){
    $_SESSION['messages'] = [];
    }
}

function create_flashmessage($type,$text){
    if(!isset($_SESSION['messages'])){
        $_SESSION['messages']=[];
    }
    $msg=[
        'type'=>$type,
        'text' => $text            
    ];
    array_push($_SESSION['messages'], $msg);           
}

function display_flashmessages(){
    if (isset($_SESSION['messages'])) {
         foreach ($_SESSION['messages'] as $msg){
        echo '<div class="alert alert-'.$msg['type'].'">'.$msg['text'] . '</div>';
    }
        $_SESSION['messages']=[];
    }
}

   
