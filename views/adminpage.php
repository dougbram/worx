<?php
require_once '../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
if (!hasRole('admin')) {
    create_flashmessage('danger', 'Not Authorized');
    header('location: ' . $us_url_root);
}
?>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <?php display_flashmessages() ?>
                <h1>Administration</h1>
                <div class="row">
                    <div class="col-sm-3">
                        <a href="<?= $us_url_root ?>views/users/list.php">
                            <button type="button" class="btn btn-default btn-block"> 
                                <h5>Users</h5>
                                <i class="fa fa-address-book-o"></i>
                            </button> 
                        </a>

                    </div>
                    <div class="col-sm-3">
                        <a href="<?= $us_url_root ?>views/roles/index.php">
                            <button type="button" class="btn btn-default btn-block">
                                <h5>Roles</h5>
                                <i class="fa fa-group"></i>
                            </button>
                        </a>
                         
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>   
    </div>

</body>
</html>
