<?php
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
if(!hasRole('admin')){
    create_flashmessage('danger', 'Not Authorized!!!');
    header('location:'. $us_url_root .'index.php');
}
if (isset($_POST['submit'])) {
    if (hasRole('admin')) {
        //check if rolename is admin or user
        if (!($_POST['rolename'] == 'user' || $_POST['rolename'] == 'admin')) {
            $result = $db->roles->insertOne([
                'name' => $_POST['rolename'],
                'description' => $_POST['description']
            ]);
            if ($result->getInsertedCount() == 1) {
                create_flashmessage('success', 'Created Role');
            }
        }
    } else {
        create_flashmessage('danger', 'Not Authorized!!');
    }
}
?>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <?php display_flashmessages(); ?>
                <h1>User Roles</h1>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Role</th>
                                <th>Description</th>
                                <th><a class="btn bnt-sm btn-primary" data-toggle="modal" data-target="#myModal">+</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $results = $db->roles->find([]);
                            foreach ($results as $role):
                                ?>
                                <tr>
                                    <td><?= $role['name'] ?></td>
                                    <td><?= $role['description'] ?></td>
                                    <td>
                                        <a href="delete.php?id=<?=$role['_id']?>"class='btn btn-danger btn-sm'>Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-sm-3"></div>
        </div> 
        <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form action="index.php" method="POST">
                            <div class="form-group">
                                <label>Role Name</label>
                                <input type="text" name="rolename" class="form-control">                                
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" name="description" class="form-control">                                
                            </div>
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

</body>
</html>
