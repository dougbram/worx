<?php
require_once '../../init.php';
if(hasRole('admin')){
    $docid = new MongoDB\BSON\ObjectId($_GET['id']);
    $document = $db->roles->findOne(['_id'=>$docid]);
    if($document['name'] == 'admin'){
        create_flashmessage('danger', 'Can Not Delete Admin user role!!');
    }else{
        $db->roles->deleteOne(['_id'=>$docid]);
    }        
}else{
    create_flashmessage('danger', 'Not Authorized');
}
header('location: index.php');

