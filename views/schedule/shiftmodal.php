<!-- The Modal -->
<div class="modal" id="myModal-<?= $sh['_id']?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Shift Options</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>
            <?= $sh['Position']. ": " . $sh['Name']  ?> 
        </p>
        <span>
          <a class="btn btn-success" href="requestcoverage.php?id=<?= $sh['_id'] ?>">Request Coverage</a>
          <a class="btn btn-primary" href="requesttrade.php?id=<?= $sh['_id'] ?>">Request Trade</a>
          <a class="btn btn-danger" href="delete.php?id=<?= $sh['_id'] ?>">Delete</a>
        </span>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>