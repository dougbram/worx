<?php
require_once '../../init.php';
$loggedinuser = $_SESSION['user'];
$propername = $loggedinuser->lastname . ", " . $loggedinuser->firstname;
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $shift = $db->shifts->findOne(['_id' => $_GET['id']]);
    //if user is admin send message
    If(hasRole('admin')){
        create_flashmessage('success','Coverage request sent');
        Redirect::to('index.php');
    }

    //if user owns shift send message
    if(!$shift['Name'] == $propername){
        create_flashmessage('success','Coverage request sent');
        Redirect::to('index.php');
            
    }

    //else deny access
    create_flashmessage('danger','Message not sent: You do not own this shift');
    Redirect::to('index.php');

}

require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';