<?php
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
$begin = new DateTime(date("m/d/y"));
if(isset($_GET['start'])){
    $begin = new DateTime($_GET['start']);
}

$end = new DateTime(date('m/t/y'));
if(isset($_GET['end'])){
    $end = new DateTime($_GET['end']);
}
date_add($end, date_interval_create_from_date_string('1 days'));
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);
$results = $db->shifts->find();

if(isset($_POST['submit'])){
    if(hasRole('admin')){
      $_shiftdate = new DateTime($_POST['shiftdate']);
      $utc_shiftdate = new DateTime($_shiftdate->format('m/d/y'),new DateTimeZone("UTC"));
      $utc_startdate = new DateTime($_POST['startdate']." ".$_POST['starttime'],new DateTimeZone("UTC"));
      $utc_enddate = new DateTime($_POST['enddate']." ".$_POST['endtime'],new DateTimeZone("UTC"));
      $data['ShiftDate'] = new Mongodb\BSON\UTCDateTime($utc_shiftdate);
      $data['StartDate'] = new Mongodb\BSON\UTCDateTime($utc_startdate);
      $data['EndDate'] = new Mongodb\BSON\UTCDateTime($utc_enddate);
      $data['Name'] = $_POST['name'];
      $data['Position'] = $_POST['position'];
      $data['TotalHours'] = $utc_enddate->diff($utc_startdate);

     $db->shifts->insertOne($data);
     //var_dump($data);
        $result = $db->shifts->insertOne($data);
        //echo ($result->getInsertedId());
        //create_flashmessage('success',"Created New Shift");
        //Redirect::to('index.php');
    }else{
        create_flashmessage('danger',"Access Denied: You must have admin role");
        Redirect::to('index.php');
    }
}
?>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm">      
        </div>
        <div class="col-sm">
            <?php display_flashmessages() ?>
            <h3 style="text-align: center">Schedule</h3>
            <div style="margin-bottom: 5px">
                <form class="form-inline" method="get">                
                    <input type="date" name="start" class="form-control" >-                
                    <input type="date" name="end" class="form-control" >
                    <button type="submit" class="btn btn-default btn-sm">Search</button>
                </form>
            </div>
            <?php if(hasRole('admin')|| hasRole('schedule')): ?>
            <div class="d-flex flex-row-reverse">
                <button type="button" class="btn btn-primary btn-md" style="margin-bottom: 5px" data-toggle="modal" data-target="#newshift_modal">+</button>                                
            </div>
            <?php endif ?>
            <?php foreach($period as $date): ?>                     
            <div class="card" style="margin-bottom: 10px">
                <div class="card-header" style="text-align: center">                    
                    <span>
                        <strong>
                        <?=$date->format('l')?>
                        <?=$date->format('m/d/y')?>
                        </strong>
                    </span>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                    <?php foreach(get_shifts($db,$date) as $sh): ?>                       
                        <li class="list-group-item">                            
                            <span>  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal-<?= $sh['_id'] ?>">                             
                                    <?= $sh['Position']. ": " . $sh['Name']  ?>
                                    </button>                                
                                   <p style="font-size: x-small"><?php echo get_local_datetime($sh['StartDate'])->format('H:i')?>
                                - <?php echo get_local_datetime($sh['EndDate'])->format('H:i')?>
                                </p>
                             </span>                            
                        </li>
                            <?php include 'shiftmodal.php' ?>
                    <?php endforeach ?>
                    </ul>
                </div> 
                <!--<div class="card-footer">Footer</div> -->
            </div>
           
            <?php endforeach ?>
        </div>
        <div class="col-sm">      
        </div>
  </div>    
</div>
<?php include 'newshiftmodal.php' ?>
</body>
</html>
