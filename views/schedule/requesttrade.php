<?php 
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';

$loggedinuser = $_SESSION['user'];
$propername = $loggedinuser->lastname . ", " . $loggedinuser->firstname;
$id= new Mongodb\BSON\ObjectID($_GET['id']);
$shift = $db->shifts->findOne(['_id' => $id]);
$isowner = false;
if($propername == $shift['Name']){
    $isowner = true;
}
$isadmin = false;
If(hasRole('admin')){
   $isadmin = true;
}
//if postback to the form send the notifications and redirect
if(isset($_POST['submit'])){
    //check if user not owner or admin redirect with error
    if($isadmin or $isowner){
        $admins = $db->users->find(['roles'=>'admin']);
        foreach($admins as $admin){
            create_flashmessage('success',"Message has been sent to".$admin['username']);
            //TODO Send actual message

        }
        Redirect::to("index.php");
    }else{
        create_flashmessage("danger","Not Authorized: You must own the shift or be an admin");
        Redirect::to('index.php');
    }
}

//if user is admin or owner then display the form
If($isadmin or $isowner): ?>
 <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                   <?php display_flashmessages() ?>
                   <h4>Trade Request</h4>
                   <form action="requesttrade.php?id=<?= $shift['_id']?>" method="post">
                        <div class="form-group">
                            <label for="startdate">Trade Start Date / Time</label>
                            <div class="input-group-prepend">
                                <input class="form-control"  type="date" name="startdate" id="startdate" value="<?=display_local_date($shift['StartDate'],1)?>">
                                <input class="form-control"  type="time" name="starttime" id="starttime" value="<?=display_local_time($shift['StartDate'])?>">
                            </div>                         
                        </div>
                        <div class="form-group">
                            <label for="enddatedate">Trade End Date / Time</label>
                            <div class="input-group-prepend">
                                <input class="form-control"  type="date" name="enddate" id="enddate" value="<?=display_local_date($shift['EndDate'],1)?>">
                                <input class="form-control"  type="time" name="endtime" id="endtime" value="<?=display_local_time($shift['EndDate'])?>">
                            </div>                 
                        </div>
                        <div class="form-group">
                            <label for="name">Name of person accepting trade</label>
                            <select class="form-control" name = "name" id="sel1">                               
                                <option></option>
                                <?php foreach(get_roster($db) as $r): ?>
                                    <option><?= $r['lastname'] . ', '.$r['firstname'] ?></option>
                                <?php endforeach ?>                                
                            </select>                           
                        </div>
                        <div class="form-group">
                            <label for="position">Position</label>
                            <select class="form-control" name = "name" id="sel1">                               
                                <option><?= $shift['Position']?></option>
                                <?php foreach(get_shift_names($db) as $r): ?>
                                    <option><?= $r['position'] ?></option>
                                <?php endforeach ?>                                
                            </select>                             
                        </div>
                        <div class="form-group">
                            <label for="notes">Notes</label>
                            <textarea class="form-control" name="notes" id="notes" cols="30" rows="10"></textarea>                           
                        </div>
                        <button type="submit" name="submit" class="btn btn-default">Submit</button>
                   </form>
                </div>
                <div class="col-sm-3"></div>
            </div>   
        </div>
    
    </body>
</html>

<?php endif ?>

