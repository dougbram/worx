<!-- The Modal -->
<div class="modal" id="newshift_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">New Shift</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <form action="index.php" method="post">
                            <div class="form-group">
                                <label for="shiftdate">Shift Date</label>                            
                                <input class="form-control"  type="date" name="shiftdate" id="shiftdate" >                                                 
                            </div>
                        <div class="form-group">
                            <label for="startdate">Start Date / Time</label>
                            <div class="input-group-prepend">
                                <input class="form-control"  type="date" name="startdate" id="startdate" >
                                <input class="form-control"  type="time" name="starttime" id="starttime">
                            </div>                         
                        </div>
                        <div class="form-group">
                            <label for="enddatedate"> End Date / Time</label>
                            <div class="input-group-prepend">
                                <input class="form-control"  type="date" name="enddate" id="enddate" >
                                <input class="form-control"  type="time" name="endtime" id="endtime" >
                            </div>                 
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <select class="form-control" name = "name" id="sel1">                               
                                <option></option>
                                <?php foreach(get_roster($db) as $r): ?>
                                    <option><?= $r['lastname'] . ', '.$r['firstname'] ?></option>
                                <?php endforeach ?>                                
                            </select>                           
                        </div>
                        <div class="form-group">
                            <label for="position">Position</label>
                            <select class="form-control" name = "position" id="sel1">                               
                                <option></option>
                                <?php foreach(get_shift_names($db) as $r): ?>
                                    <option><?= $r['position'] ?></option>
                                <?php endforeach ?>                                
                            </select>                             
                        </div>                        
                        <button type="submit" name="submit" class="btn btn-default">Submit</button>
                   </form>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>