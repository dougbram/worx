<?php
require_once '../../init.php';
if(isset($_GET['id'])){
    $id= new Mongodb\BSON\ObjectID($_GET['id']);
    if(hasrole('admin')){
        $result = $db->shifts->deleteOne(['_id'=>$id]);
        create_flashmessage('success',"Shift Deleted");
        Redirect::to('index.php');
    }else{
        create_flashmessage('danger','Access Denied: Not authorized');
        Redirect::to('index.php');
    }
}
