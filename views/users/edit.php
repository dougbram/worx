<?php
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
if(!hasRole('admin')){
    create_flashmessage('danger', 'Not Authorized!!!');
    header('location:'. $us_url_root .'index.php');
}
if (isset($_GET['id'])) {
    $modifyuser = new User();
    $modifyuser = $db->users->findOne(['_id' => new MongoDB\BSON\ObjectId($_GET['id'])]);
}

if (isset($_POST['submit'])) {
    $roles = [];
    foreach($_POST['roles'] as $r){
        array_push($roles, $r);
    }
    $data = [
        'username' => filter_var($_POST['username'], FILTER_SANITIZE_STRING),
        'firstname' => filter_var($_POST['firstname'], FILTER_SANITIZE_STRING),
        'lastname' => filter_var($_POST['lastname'], FILTER_SANITIZE_STRING),
        'cellphone' => filter_var($_POST['cellphone'], FILTER_SANITIZE_STRING),
        'email' => filter_var($_POST['email'], FILTER_SANITIZE_EMAIL),
        'certification' => filter_var($_POST['certification'], FILTER_SANITIZE_STRING),
        'roles' => $roles
    ];
    $result = $db->users->updateOne(['_id' => new MongoDB\BSON\ObjectId($_GET['id'])], ['$set' => $data]);
    header('location: list.php');
}
if(isset($_POST['resetpw'])){
    if(!validate_password($_POST['password'], $_POST['confirmpassword'])){
        //passwords match
        $data = [
            'password'=> password_hash($_POST['password'], PASSWORD_DEFAULT)
        ];
        //update database
        $result=$db->users->updateOne(['_id' => new MongoDB\BSON\ObjectId($_GET['id'])], ['$set' => $data]);
        if($result->getModifiedCount() == 1){
            create_flashmessage('success', 'Password has been reset');
        }else{
            create_flashmessage('danger', 'Error occurred while resetting password');
        }
              
    }else{
        create_flashmessage('danger', 'The passwords do not match');
    }    
}
?>
<body>   
    <div class="container-fluid">        
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <?php display_flashmessages() ?>
                <h3>Edit User</h3>
                <form action="edit.php?id=<?= $_GET['id'] ?>" method="POST">
                    <div class="form-group">
                        <label for="Username">User Name</label>
                        <input type="text" name="username" class="form-control" value="<?= $modifyuser->username ?>">
                    </div>
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" name="firstname" class="form-control" value="<?= $modifyuser->firstname ?>">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" name="lastname" class="form-control" value="<?= $modifyuser->lastname ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" class="form-control" value="<?= $modifyuser->email ?>">
                    </div>
                    <div class="form-group">
                        <label for="cellphone">Cell Phone</label>
                        <input type="tel" name="cellphone" class="form-control" value="<?= $modifyuser->cellphone ?>">
                    </div>
                    <div class="form-group">
                        <label for="certification">Certification</label>
                        <input type="text" name="certification" class="form-control" value="<?= $modifyuser->certification ?>">
                    </div>
                    <div class="form-group">
                        <label>Roles: </label>
                    <?php $roleresult = $db->roles->find([]); foreach($roleresult as $role ): ?>
                        <label class="checkbox-inline"><input type="checkbox" name="roles[<?=$role['name']?>]" value="<?=$role['name']?>" <?php if(userHasRole($modifyuser, $role['name']))echo 'checked' ?>><?=$role['name']?></label>
                    <?php endforeach; ?>
                    </div>
                    <button typ="submit" name="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Reset Pasword</button>
                </form>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form action="edit.php?id=<?= $_GET['id'] ?>" method="POST">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="confirmpassword">Confirm Password</label>
                                <input type="password" name="confirmpassword" class="form-control">
                            </div>
                            <button typ="submit" name="resetpw" class="btn btn-success">Submit</button>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</html>

