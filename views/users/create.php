<?php
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
if(!hasRole('admin')){
    create_flashmessage('danger', 'Not Authorized!!!');
    header('location:'. $us_url_root .'index.php');
}
if(isset($_POST['submit'])){
    //process password
   if(validate_password($_POST['password'], $_POST['confirmpassword'])){
        create_flashmessage("danger", "Passwords do not match");
        Redirect::to('list.php');
   }
   
   //find if username is unique
   $collection = (new MongoDB\Client)->$dbname->users;
   $cursor = $collection->find([],['projection' =>['username'=>1]]);
   if(validate_username($_POST['username'], $cursor)){
       create_flashmessage('danger', 'Username already exists');
       Redirect::to('list.php');
   }
   
  $newuser = new User();
  $newuser->username = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
  $newuser->firstname = filter_var($_POST['firstname'],FILTER_SANITIZE_STRING);
  $newuser->lastname = filter_var($_POST['lastname'],FILTER_SANITIZE_STRING);
  $newuser->cellphone = filter_var($_POST['cellphone'],FILTER_SANITIZE_STRING);
  $newuser->email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
  $newuser->certification = filter_var($_POST['certification'],FILTER_SANITIZE_STRING);
  foreach($_POST['roles'] as $r){
      array_push($newuser->roles, $r);
      } 
  $newuser->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
 $result = $db->users->insertOne($newuser);
 if($result->getInsertedCount() == 1){
     create_flashmessage('success', "User Added");
     Redirect::to('list.php');
 }else{
     create_flashmessage('danger','Error Adding User');
     Redirect::to('list.php');
 }
}

?>
<body>   
    <div class="container-fluid">        
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <?php display_flashmessages() ?>
                <h3>New User</h3>
                <form action="create.php" method="POST">
                    <div class="form-group">
                        <label for="Username">User Name</label>
                        <input type="text" name="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" name="firstname" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" name="lastname" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="cellphone">Cell Phone</label>
                        <input type="tel" name="cellphone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="certification">Certification</label>
                        <input type="text" name="certification" class="form-control">
                    </div>       
                    <label>Roles: </label>
                    <?php $roleresult = $db->roles->find([]); foreach($roleresult as $role ): ?>
                        <label class="checkbox-inline"><input type="checkbox" name="roles[<?=$role['name']?>]" value="user"><?=$role['name']?></label>
                    <?php endforeach; ?>
                     <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="confirmpassword">Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control">
                    </div>
                    <button typ="submit" name="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</html>