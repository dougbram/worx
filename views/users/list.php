<?php
require_once '../../init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
if(!hasRole('admin')){
    create_flashmessage('danger', 'Not Authorized!!!');
    header('location:'. $us_url_root .'index.php');
}
$collection = (new MongoDB\Client)->$dbname->users;
$cursor = $collection->find();
?>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <?= display_flashmessages() ?>
                <h1>Users</h1>
                <div>
                    <a href="create.php" class="btn btn-success btn-special">New User</a>
                </div>
                <?php foreach ($cursor as $doc): ?>
                    <div class="card">
                        <div class="card-header"><?php echo $doc['firstname'] . " " . $doc['lastname'] . " - " . $doc['certification'] ?></div>
                        <div class="card-body">
                            <ul>
                                <li><strong>Username: </strong><?php echo $doc['username'] ?></li>
                                <li><strong>Cellphone: </strong><?php echo $doc['cellphone'] ?></li>
                                <li><strong>Email: </strong><?php echo $doc['email'] ?></li>
                            </ul>
                                            
                        </div> 
                        <div class="card-footer">
                            <p>
                                <a class="btn btn-primary" href="edit.php?id=<?=$doc['_id']?>">Edit</a>
                                <a class="btn btn-danger" href="delete.php?id=<?=$doc['_id']?>">Delete</a>
                            </p>
                        </div>
                    </div>
                <?php endforeach ?>

            </div>
            <div class="col-sm-3"></div>
        </div>   

    </div>

</body>
</html>