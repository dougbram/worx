<?php

require_once '../../init.php';
if(hasRole('admin')){
    if(isset($_GET['id'])){
        $result = $db->users->deleteOne(['_id'=> new MongoDB\BSON\ObjectId($_GET['id'])]);
        if($result->getDeletedCount() == 1){
        create_flashmessage('success', 'Deleted 1 user');
    }else{
        create_flashmessage('danger', 'An error occurred');
    }
    }
}else{
    create_flashmessage('danger', 'Not authorized!');
}

header('location:list.php');

