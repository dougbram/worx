<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="<?= $us_url_root ?>"><?= $appname ?></a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav mr-auto">
            <?php if (hasRole('admin')): ?>                
                <li class="nav-item">
                    <a class="nav-link" href="<?= $us_url_root ?>views/adminpage.php">Admin</a>
                </li>
            <?php endif ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $us_url_root ?>views/schedule/index.php">Schedule</a>
                </li>
        </ul>
        <ul class="navbar-nav">
           <?php if (!isLoggedIn()): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $us_url_root ?>login.php">Log In</a>
                </li>
            <?php else: ?>
                <li class="nav-item">
                    <a class="nav-link" href="#"><?=$_SESSION['user']->username?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $us_url_root ?>/logout.php">Log out</a>
                </li>
            <?php endif ?>
        </ul>
    </div> 
</nav>