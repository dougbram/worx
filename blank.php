<?php
require_once 'init.php';
require_once $abs_us_root . $us_url_root . 'views/header.php';
require_once $abs_us_root . $us_url_root . 'views/navbar.php';
$pagename = "blank";

?>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                   <?php display_flashmessages() ?>
                </div>
                <div class="col-sm-3"></div>
            </div>   
        </div>
    
    </body>
</html>
