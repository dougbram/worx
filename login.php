<?php 
require_once 'init.php';
require_once $abs_us_root.$us_url_root.'views/header.php';
require_once $abs_us_root.$us_url_root.'views/navbar.php';

if(isset($_POST['submit'])){   
    $document = $db->users->findOne(['username' => $_POST['username']]);
    $hashedpw = $document['password'];
     // authenticate password
    if(password_verify($_POST['password'], $hashedpw)){
       
         $authuser = new User;
         $authuser->login($document);
         $_SESSION['user']= $authuser;
         header('location: index.php');
    }else{
        create_flashmessage('danger', 'Incorrect username or password');
    }   
}

?>
<body>

<div class="container-fluid">
    <div class="row"></div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <?php display_flashmessages() ?>
            <form action="login.php" method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text"class="form-control" name="username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password"class="form-control" name="password">
                </div>
                <input type="submit" value="submit" name="submit" class="btn btn-success">
            </form>
        </div>
        <div class="col"></div>
    </div>
    <div class="row"></div>
 
</div>

</body>
</html>